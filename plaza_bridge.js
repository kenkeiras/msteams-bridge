const WebSocket = require('isomorphic-ws');

const RECONNECT_TIME = 5000;  // ms

function assert_type(value, type, check) {
    if ((check && (!check())) ||
        (!check && typeof(value) !== type)) {
        throw Error("Found “" + value + "” expected type " + type);
    }
}

class PlazaBridge {
    constructor (name, is_public, events,
                 // Optional for now (checked on `run()`)
                 bridge_endpoint, event_handler) {
        assert_type(name, 'string');
        this.name = name;
        assert_type(is_public, 'boolean');
        this.is_public = is_public;
        assert_type(events, 'list', () => typeof(events.length) === 'number');
        this.events = events;

        this.bridge_endpoint = bridge_endpoint;
        this.event_handler = event_handler;
        this.websocket = undefined;
    }

    set_event_handler(event_handler) {
        this.event_handler = event_handler;
    }

    run() {
        if (!this.bridge_endpoint) {
            throw Error("Invalid endpoint: " + this.bridge_endpoint);
        }
        if (!this.event_handler) {
            throw Error("Invalid event handler: " + this.event_handler);
        }
        this._connect();
    }

    _connect() {
        const bridge = this;
        this.websocket = new WebSocket(this.bridge_endpoint);

        this.websocket.onopen = () => {
            console.log('connected');
            this._send_configuration();
        };

        this.websocket.onclose = () => {
            console.log('disconnected');
            setTimeout(bridge._connect, RECONNECT_TIME);
        };

        this.websocket.onmessage = this._on_message;
    }

    _send_configuration() {
        console.log("Send", this.websocket.send(JSON.stringify({
            "type": "CONFIGURATION",
            "value": { 
                "blocks": [  ],
                "is_public": this.is_public,
                "service_name": this.name,
            }
        })));
    }

    _on_message(data) {
        console.log("New message");
    }
}

module.exports.PlazaBridge = PlazaBridge;
